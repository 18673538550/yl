package com.sz.oauth2.config;

import com.sz.oauth2.service.impl.UserSecurityServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Author yong.li
 * @Date 2020/8/3
 **/
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 继承安全组件中的UserDetailsService
     * 重写loadUserByUsername方法
     *
     * @return
     */
    @Bean
    public UserDetailsService UserSecurityService() {
        return new UserSecurityServiceImpl();
    }

    /**
     * 使用BCrypt加密
     *
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


//    public static void main(String[] args) {
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//        String password = bCryptPasswordEncoder.encode("admin");
//        System.out.println("password:"+password);
//    }
    /**
     * 重写授权配置
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(UserSecurityService()).passwordEncoder(passwordEncoder());
    }


    /**
     * 重写授权管理Bean,让授权服务类AuthorizationServerConfig进行bean的加载
     *
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * http安全配置
     * hasRole("USER") 对应数据库值 ROLE_USER
     *
     * @param http http安全对象
     * @throws Exception http安全异常信息
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //案例一
//        http.authorizeRequests()
//                .antMatchers("/css/**","/static/**").permitAll()
//                .antMatchers("/user/**").hasRole("USER")
//                .antMatchers("/oauth/**").hasAnyRole("ADMIN","USER")
//                //.anyRequest().authenticated()
//                .and().formLogin().loginPage("/login").failureUrl("/login-error")
//                .and().exceptionHandling().accessDeniedPage("/401");
//        http.logout().logoutSuccessUrl("/");
        //案例二
//        http.authorizeRequests()
//                .requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
//                .antMatchers("/rsa/publicKey").permitAll()
//                .anyRequest().authenticated();
        http.formLogin()
                .loginPage("/login")
                .and()
                .authorizeRequests()
                .antMatchers("/.well-known/jwks.json").permitAll()
                .antMatchers("/login").permitAll()
                .anyRequest()
                .authenticated()
                .and().csrf().disable().cors();
    }
}
