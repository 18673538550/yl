package com.sz.oauth2.dto;

import com.sz.oauth2.entity.SysRole;
import com.sz.oauth2.entity.SysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author yong.li
 * @Date 2020/8/4
 **/
public class UserSecurityDto implements UserDetails {

    private SysUser sysUser;
    private List<SysRole> sysRoles;

    public UserSecurityDto(SysUser sysUser, List<SysRole> sysRoles) {
        this.sysUser = sysUser;
        this.sysRoles = sysRoles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return sysRoles.stream().map(sysRole -> new SimpleGrantedAuthority(sysRole.getName())).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return sysUser.getPassword();
    }

    @Override
    public String getUsername() {
        return sysUser.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return 1==sysUser.getStatus();
    }
}
