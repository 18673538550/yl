package com.sz.oauth2.service.impl;

import com.sz.oauth2.entity.SysUserRole;
import com.sz.oauth2.mapper.SysUserRoleMapper;
import com.sz.oauth2.service.SysUserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户与角色对应关系 服务实现类
 * </p>
 *
 * @author yl
 * @since 2020-08-04
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}
