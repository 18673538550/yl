package com.sz.oauth2.service;

import com.sz.oauth2.entity.SysUserRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户与角色对应关系 服务类
 * </p>
 *
 * @author yl
 * @since 2020-08-04
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
