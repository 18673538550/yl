package com.sz.oauth2.service;

import com.baomidou.mybatisplus.service.IService;
import com.sz.oauth2.entity.SysRole;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author yl
 * @since 2020-08-04
 */
public interface SysRoleService extends IService<SysRole> {
    /**
     * 查询用户角色列表
     * @param uid
     * @return
     */
    public List<SysRole> queryRoleByUid(Long uid);
}
