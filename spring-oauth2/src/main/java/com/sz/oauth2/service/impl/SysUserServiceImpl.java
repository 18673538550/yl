package com.sz.oauth2.service.impl;

import com.sz.oauth2.entity.SysUser;
import com.sz.oauth2.mapper.SysUserMapper;
import com.sz.oauth2.service.SysUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author yl
 * @since 2020-08-03
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
