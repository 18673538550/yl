package com.sz.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author yong.li
 * @Date 2020/6/3
 **/
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.sz")
public class Oauth2Application {


    public static void main(String[] args) {
        SpringApplication.run(Oauth2Application.class);
    }
}
