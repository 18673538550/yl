package com.sz.cm.common;

import lombok.Data;

/**
 * @Author yong.li
 * @Date 2020/7/16
 **/
@Data
public class ResponseEntity<T> {
    /**
     * 状态码
     */
    private int code;
    /**
     * 描述
     */
    private String msg;
    /**
     * 结果集
     */
    private T data;

    public ResponseEntity(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResponseEntity(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
