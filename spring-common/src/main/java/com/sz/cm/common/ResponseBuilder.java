package com.sz.cm.common;

import com.sz.cm.enums.ResponseEnum;

/**
 * @Author yong.li
 * @Date 2020/7/16
 **/
public class ResponseBuilder {


    public static <T> ResponseEntity buildBusSuccessResult(T data) {
        return new ResponseEntity(ResponseEnum.RE_SUCCESS.getCode(), ResponseEnum.RE_SUCCESS.getMsg(), data);
    }

    public static ResponseEntity buildBusSuccessResult() {
        return new ResponseEntity(ResponseEnum.RE_SUCCESS.getCode(), ResponseEnum.RE_SUCCESS.getMsg());
    }

    public static ResponseEntity buildBusErrorResult() {
        return new ResponseEntity(ResponseEnum.RE_ERROR.getCode(), ResponseEnum.RE_ERROR.getMsg());
    }

    public static <T> ResponseEntity buildBusErrorResult(T data) {
        return new ResponseEntity(ResponseEnum.RE_ERROR.getCode(), ResponseEnum.RE_ERROR.getMsg(), data);
    }

    public static ResponseEntity buildBusFailResult() {
        return new ResponseEntity(ResponseEnum.RE_FAIL.getCode(), ResponseEnum.RE_FAIL.getMsg());
    }

    public static <T> ResponseEntity buildResult(int code, String msg, T data) {
        return new ResponseEntity(code, msg, data);
    }

    public static ResponseEntity buildResult(int code, String msg) {
        return new ResponseEntity(code, msg);
    }

    public static ResponseEntity buildResult(ResponseEnum ResultEnum) {
        return new ResponseEntity(ResultEnum.getCode(), ResultEnum.getMsg());
    }

}
