package com.sz.cm.exception;

import com.sz.cm.common.ResponseBuilder;
import com.sz.cm.common.ResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 基本全局异常处理器
 *
 * @Author yong.li
 * @Date 2020/7/17
 **/

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BusinessException.class)
    public ResponseEntity handleBusiness(BusinessException e) {
        log.error("BusinessException异常捕获:{}", e.getMessage());
        return ResponseBuilder.buildBusErrorResult(e.getMessage());
    }
}
