package com.sz.cm.exception;

import com.sz.cm.common.ResponseEntity;
import lombok.Getter;

/**
 * 自定义网关异常
 *
 * @Author yong.li
 * @Date 2020/7/18
 **/
@Getter
public class GatewayException extends RuntimeException {


    private ResponseEntity responseEntity;

    public GatewayException(int code, String message) {
        responseEntity = new ResponseEntity(code, message);
    }
}
