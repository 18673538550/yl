package com.sz.cm.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.net.URL;
import java.util.Date;

/**
 * @author : Yong.Li
 * @date : 2019/12/31 13:32
 */
@Slf4j
public class OSSUtils {

    /**
     * 简单上传
     *
     * @param endpoint
     * @param accessKeyId
     * @param accessKeySecret
     * @param bucketName
     * @param targetPath
     * @param file
     */
    public static boolean putObject(String endpoint, String accessKeyId, String accessKeySecret, String bucketName, String targetPath, File file) {
        boolean isSuccess = false;
        OSS ossClient = null;
        try {
            // 创建OSSClient实例。
            ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, targetPath, file);
            // 上传文件
            ossClient.putObject(putObjectRequest);
            // 结果标识
            isSuccess = true;
        } catch (Exception e) {
            log.error("上传阿里云OSS时,发生异常,文件:{}", file.getPath(), e);
            return isSuccess;
        } finally {
            // 关闭OSSClient
            if (null != ossClient) {
                ossClient.shutdown();
            }
        }
        return isSuccess;
    }

    /**
     * 简单上传，后返回可下载的路径
     *
     * @param endpoint
     * @param accessKeyId
     * @param accessKeySecret
     * @param bucketName
     * @param targetPath
     * @param file
     * @Param expiration
     */
    public static boolean putObjectByAttachment(String endpoint, String accessKeyId, String accessKeySecret, String bucketName, String targetPath, File file, Date expiration) {
        boolean isSuccess = false;
        // 创建OSSClient实例。
        OSS ossClient = null;
        try {
            ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            // 如果需要上传时设置存储类型与访问权限，请参考以下示例代码。
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentDisposition("attachment;");

            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, targetPath, file, meta);
            // 上传文件
            ossClient.putObject(putObjectRequest);

            // 设置URL过期时间为1小时。
            //Date expiration = new Date(System.currentTimeMillis() + 3600 * 1000);
            // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
            URL url = ossClient.generatePresignedUrl(bucketName, targetPath, expiration);
            log.info("阿里云OSS返回的可下载路径:{}", url.toString());
            // 结果标识
            isSuccess = true;
        } catch (Exception e) {
            log.error("上传阿里云OSS时,发生异常,文件:{}", file.getPath(), e);
            return isSuccess;
        } finally {
            // 关闭OSSClient
            if (null != ossClient) {
                ossClient.shutdown();
            }
        }

        return isSuccess;
    }
}
