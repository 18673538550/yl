package com.sz.gateway.support;

import com.sz.cm.enums.ResponseEnum;
import com.sz.gateway.util.CommonUtil;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.ServerAuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 *
 * 自定义返回结果：没有登录或token过期时
 * @Author yong.li
 * @Date 2020/8/20
 **/
@Component
public class RestfulAuthenticationEntryPoint implements ServerAuthenticationEntryPoint {

    @Override
    public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException e) {
        ServerHttpResponse response = exchange.getResponse();
        DataBuffer buffer = CommonUtil.getBody(response,ResponseEnum.RE_UNAUTHORIZED);
        return response.writeWith(Mono.just(buffer));
    }
}
