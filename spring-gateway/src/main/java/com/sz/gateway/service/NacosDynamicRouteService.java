package com.sz.gateway.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionWriter;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * @Author yong.li
 * @Date 2020/6/19
 **/
@Slf4j
@Component
@RefreshScope
public class NacosDynamicRouteService implements ApplicationEventPublisherAware {


    @Value("${spring.cloud.gateway.dataId}")
    private String dataId;
    @Value("${spring.cloud.gateway.group}")
    private String group;
    @Value("${spring.cloud.gateway.namespace}")
    private String namespace;
    @Value("${spring.cloud.nacos.config.server-addr}")
    private String serverAddr;

    private static final List<String> routeList = new ArrayList<>();

    @Autowired
    private RouteDefinitionWriter routeDefinitionWriter;
    private ApplicationEventPublisher applicationEventPublisher;

    @PostConstruct
    public void dynamicRouteListener() {
        try {
            Properties properties = new Properties();
            properties.setProperty("serverAddr",serverAddr);
            properties.setProperty("namespace",namespace);
            ConfigService configService = NacosFactory.createConfigService(properties);
            String config = configService.getConfig(dataId, group, 5000);
            log.info("gateway动态路径JSON:{}",config);
            configService.addListener(dataId, group, new Listener() {
                @Override
                public Executor getExecutor() {
                    return null;
                }

                @Override
                public void receiveConfigInfo(String configInfo) {
                    clearRoute();
                    List<RouteDefinition> routeDefinitions = JSONArray.parseArray(configInfo, RouteDefinition.class);
                    for (RouteDefinition route : routeDefinitions) {
                        addRoute(route);
                    }
                    publish();
                }
            });
        } catch (Exception e) {

        }
    }

    private void clearRoute() {
        for (String id : routeList) {
            this.routeDefinitionWriter.delete(Mono.just(id)).subscribe();
        }
        routeList.clear();
    }


    private void publish() {
        this.applicationEventPublisher.publishEvent(new RefreshRoutesEvent(this.routeDefinitionWriter));
    }

    private void addRoute(RouteDefinition route) {
        try {
            routeDefinitionWriter.save(Mono.just(route)).subscribe();
            routeList.add(route.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
