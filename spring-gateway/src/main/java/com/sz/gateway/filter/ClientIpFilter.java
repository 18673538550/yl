package com.sz.gateway.filter;

import com.sz.cm.exception.GatewayException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.InetSocketAddress;

/**
 * 请求IP过滤器
 *
 * @Author yong.li
 * @Date 2020/7/17
 **/
@Slf4j
public class ClientIpFilter implements GlobalFilter, Ordered {


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        InetSocketAddress inetSocketAddress = request.getRemoteAddress();
        String clientIp = inetSocketAddress.getAddress().getHostAddress();
        log.info("IP:{}", clientIp);
        if(clientIp.equals("10.83.8.111")){
            throw new GatewayException(555,"大哥你非法访问了,赶紧去授权吧");
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
