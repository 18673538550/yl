package com.sz.gateway.util;

import cn.hutool.json.JSONUtil;
import com.sz.cm.common.ResponseBuilder;
import com.sz.cm.enums.ResponseEnum;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;

import java.nio.charset.Charset;

/**
 * @Author yong.li
 * @Date 2020/8/20
 **/
public class CommonUtil {

    /**
     * 包装返回内容
     * @param response
     * @param responseEnum
     * @return
     */
    public static DataBuffer getBody(ServerHttpResponse response, ResponseEnum responseEnum) {
        response.setStatusCode(HttpStatus.OK);
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        String body = JSONUtil.toJsonStr(ResponseBuilder.buildResult(responseEnum));
        DataBuffer buffer = response.bufferFactory().wrap(body.getBytes(Charset.forName("UTF-8")));
        return buffer;
    }
}
